// 首先使用npm安装

// npm install qiniu-js

// 引入

import * as qiniu from 'qiniu-js';
let subscription = '';
// 直接上代码'
function Preview(event) {
    let that = this;
    let file = event.target.files[0];
    let observable;
    if (file.size > 1024 * 1024 * 200) {
        that.sizeTopic = true;
        setTimeout(function() {
            that.sizeTopic = false;
        }, 1000);
        return false;
    }
    let name = event.target.files[0].name;
    this.resources_name = name;
    this.sendSuccess = true;
    this.$http
        .get(process.env.API_ROOT + 'api/sqiniu/uptoken?name=' + name)
        .then(res => {
            let token = res.data.data.uptoken; //上传验证信息，前端通过接口请求后端获得
            //七牛API
            let config = {
                useCdnDomain: true,
                disableStatisticsReport: false,
                retryCount: 5,
                region: qiniu.region.z2
            };
            let putExtra = {
                fname: '',
                params: {},
                mimeType: null
            };
            let key = file.name;
            // 添加上传dom面板
            let next = response => {
                let total = response.total; //上传进度提示
                that.percent = parseInt(total.percent);
            };
            let complete = res => {
                (that.resources_key = res.key),
                    (that.resources_hash = res.hash),
                    (that.resources_size = res.fsize),
                    (that.resources_type = res.type);
                that.sendTopic = true;
                setTimeout(function() {
                    that.sendTopic = false;
                }, 1000);
            };
            //七牛云上传对象
            let subObject = {
                next: next,
                complete: complete,
                error: error
            };
            // let subscription;
            let error = err => {
                that.failTopic = true;
                setTimeout(function() {
                    that.failTopic = false;
                }, 1000);
            };
            // 调用sdk上传接口获得相应的observable，控制上传和暂停
            observable = qiniu.upload(file, key, token, putExtra, config);
            this.subscription = observable.subscribe(subObject);
        })
        .catch(function(error) {
            console.log(error);
        });
}

this.subscription.unsubscribe(); //取消上传方法
